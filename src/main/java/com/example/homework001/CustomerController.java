package com.example.homework001;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    List<Customer> customerList = new ArrayList<>();
    int id = 1;
    public CustomerController(){
        customerList.add(new Customer(id++,"elisa","Female",29,"PP"));
        customerList.add(new Customer(id++,"robert","male",19,"BTB"));
        customerList.add(new Customer(id++,"nary","Female",25,"SR"));
    }

    // get all data from array list
    @GetMapping("/customer")
    public ResponseEntity<?> getAllCustomer(){
        if(customerList.isEmpty()){
            return ResponseEntity.ofNullable("There is no data");
        }
        return ResponseEntity.ok(customerList);
    }

    // insert into array list "customerList"
    @PostMapping("/customer")
    public Customer insertNewCustomer(@RequestBody CustomerRequest customerRequest){
        Customer customer = new Customer();
        customer.setId(id++);
        customer.setName(customerRequest.getName());
        customer.setAge(customerRequest.getAge());
        customer.setGender(customerRequest.getGender());
        customer.setAddress(customerRequest.getAddress());
        customerList.add(customer);
        return customer;
    }


    @GetMapping("/customer/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Integer cusId){
        boolean found = false;
        for (Customer cus: customerList){
            if (cus.getId() == cusId){
                found = true;
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has found successfully😍😍",
                        cus,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return  ResponseEntity.ofNullable("Id Input not Found 😥😥");
    }

    // Read customer by name
    @GetMapping("/customer/searchByName")
    public ResponseEntity<?> readCustomerByName(@RequestParam String name){
        boolean found = false;
        for (Customer cus: customerList){
            if (cus.getName().equals(name)){
                found = true;
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has found successfully 😍😍",
                        cus,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
            return ResponseEntity.badRequest().body("Id Input not Found 😥😥");
    }

    @PutMapping("/customer/{id}")
    public ResponseEntity<?> updateCustomerById(
            @RequestBody CustomerRequest customerRequest,
            @PathVariable Integer id) {
            for (int i=0; i<customerList.size();i++) {
                if (customerList.get(i).getId() == id) {
                    Customer customer = new Customer();
                    customer.setId(id);
                    customer.setName(customerRequest.getName());
                    customer.setAge(customerRequest.getAge());
                    customer.setGender(customerRequest.getGender());
                    customer.setAddress(customerRequest.getAddress());
                    customerList.set(id-1,customer);
                    return ResponseEntity.ok(new CustomerResponse<>(
                            "You're update successfully 😍😍",
                            customer,
                            "OK",
                            LocalDateTime.now()
                    ));
                }
        }
            return ResponseEntity.badRequest().body(
                    "Id Input not Found 😥😥"
            );
    }


    @DeleteMapping("/customer/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable("id") int cusId) {
        boolean found = false;
        for (Customer cus : customerList) {
            if (cus.getId() == cusId) {
                found = true;
                customerList.remove(cus);
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has delete successfully 😍😍",
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.badRequest().body(
                "Id Input Not Found...😥😥"
        );
    }
}
