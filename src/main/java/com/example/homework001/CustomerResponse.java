package com.example.homework001;

import java.time.LocalDateTime;

public class CustomerResponse<T> {
    private String message;
    private T customer;
    private String status;
    private LocalDateTime dateTime;


    public CustomerResponse( String message, T customer, String status,LocalDateTime dateTime) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.dateTime = dateTime;
    }

    public CustomerResponse(String message, String status, LocalDateTime dateTime){
        this.message = message;
        this.status = status;
        this.dateTime = dateTime;
    }

    public CustomerResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }
}
